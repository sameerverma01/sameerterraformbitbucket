variable "countval"{}

resource "aws_instance" "myec2" {
  ami = "ami-00dc79254d0461090"
  instance_type = "${lookup(var.instancetype, terraform.workspace)}"
  count = var.countval
}
